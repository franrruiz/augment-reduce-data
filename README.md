# Augment and Reduce: Datasets

This repo contains the datasets used in the paper
+ Francisco J. R. Ruiz, Michalis K. Titsias, Adji B. Dieng, and David M. Blei. *Augment and Reduce: Stochastic Inference for Large Categorical Distributions*. International Conference on Machine Learning. 2018.

You can download the code from [this repo](https://github.com/franrruiz/augment-reduce). Please cite this paper if you use this software.

## Datasets

The datasets were obtained from the following links:

+ MNIST was downloaded from [here](http://yann.lecun.com/exdb/mnist).

+ Omniglot was downloaded from [here](https://github.com/brendenlake/omniglot).

+ Bibtex, EURLex-4K, and AmazonCat-13K were obtained from [here](http://manikvarma.org/downloads/XC/XMLRepository.html).

Please refer to these resources for further details including citation requirements.
